import { Provider } from 'mobx-react';
import * as React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { SearchImgInInst } from '../modules/Main/pages/searchImgInInst/searchImgInInstComponent';
import MainWrap from '../modules/Wrappers/mainWrap/mainWrapComponent';
import globalStore from '../store/globalStore';

const stores = { globalStore };

export default class MainRouter extends React.Component<{
    history?: any;
    location?: any;
}> {
    render() {
        return (
            <Provider
                {...stores}
            >
                <HashRouter>
                    <Switch>
                        <MainWrap
                            location={this.props.location}
                        >
                            <Route
                                path="/search-images"
                                component={SearchImgInInst}
                            />
                        </MainWrap>
                    </Switch>
                </HashRouter>
            </Provider>
        );
    }
}
