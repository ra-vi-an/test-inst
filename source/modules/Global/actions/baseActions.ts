export class BaseActions {
    static apiResource(url: string, params?: FormData): Promise<any> {
        let parameters: string = '';
        let userId: string = '17841424904005941';
        let token: string = 'EAAnCf5AoGugBAMOZAXNiXzLR6RKTHSJr6ZCRZCPWLAn6IwEI4vMcal1ydyy4IRRl3k4F0CvrlR2aaRFVfpY0Syq0ZAmtYDIxCC3YYNdZBxPh7BnjDhcFqDtf2hneUADkSisldKNzuabJIqtE0PfGBvJmrUb6YArRlf2DQ4dFxgDyvMXkg13ro';
        let formData: FormData | undefined = params;
        formData?.append('access_token', token);
        formData?.append('user_id', userId);

        params?.forEach((item: string, key: string) => {
            parameters += `${key}=${item}&`
        })
        parameters = parameters.substring(0, parameters.length - 1);
        return fetch(`https://graph.facebook.com/v11.0/${url}/?${parameters}`)
            .then(res => res.json());
    }
}
