import { WsReactBaseComponent } from '@thewhite/react-base-components';
import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import classnames from 'classnames';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import STATUS from '../../enums/status';
import TOAST from '../../enums/toast';

interface IToastProps {
    showToast?: boolean;
    status: string | null;
    massage?: string | null;
    globalStore?: GlobalStore;
}

interface IToastState {
    showToast: boolean;
}

interface IToast {
    props: IToastProps;
    state: IToastState;
}

@inject('globalStore')
@observer
export default class Toast extends WsReactBaseComponent<IToastProps, IToastState> implements IToast {
    state: IToastState = {
        showToast: false,
    }

    componentDidMount() {
        this.checkStatus();
    };

    checkStatus = (): boolean => {
        return this.props.status === TOAST.SUCCESS.value;
    };

    render(): false | JSX.Element {
        return (
           <div
                className={classnames(
                    this.props.showToast ? 'toast' : 'toast__hidden',
                    this.checkStatus() ? 'toast__success' : 'toast__error',
                )}
           >
               <h3>
                    {
                        !!TOAST[this.props.status ?? '']
                            ? TOAST[this.props.status ?? ''].name
                            : ''
                    }
               </h3>
               <h4>
                   {this.props.massage}
               </h4>
           </div>
        );
    }
}
