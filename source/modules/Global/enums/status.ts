import { TypeService } from '@thewhite/react-global-module/lib/services';

export enum statusEnum {
    ERROR = 'ERROR',
    SUCCESS = 'SUCCESS',
}

export interface IStatus {
    [value: string]: string;

    ERROR: string;
    SUCCESS: string;
}

/**
 * Статус
 */
const STATUS: IStatus = {
    ERROR: 'error',
    SUCCESS: 'success',
};

export default STATUS;

export const StatusType = TypeService.createEnum<statusEnum>(statusEnum, 'statusEnum');
