import { SimpleObjectInterface } from '@thewhite/react-global-module/lib/interfaces';
import { TypeService } from '@thewhite/react-global-module/lib/services';

export enum toastEnum {
    ERROR = 'ERROR',
    SUCCESS = 'SUCCESS',
}

export interface IToast {
    [value: string]: SimpleObjectInterface;

    ERROR: SimpleObjectInterface;
    SUCCESS: SimpleObjectInterface;
}

/**
 * Статус
 */
const TOAST: IToast = {
    ERROR: {
        name: 'Ошибка',
        value: 'ERROR',
    },
    SUCCESS: {
        name: 'Выполнено',
        value: 'SUCCESS',
    },
};

export default TOAST;

export const ToastType = TypeService.createEnum<toastEnum>(toastEnum, 'toastEnum');
