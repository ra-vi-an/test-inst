import { TypeService } from '@thewhite/react-global-module/lib/services';

export enum mediaTypeEnum {
    IMAGE = 'IMAGE',
}

export interface MediaType {
    [value: string]: string;

    IMAGE: string;
}

/**
 * Тип медиа
 */
const MEDIA_TYPE: MediaType = {
    IMAGE: 'IMAGE',
};

export default MEDIA_TYPE;

export const MediaTypeEnum = TypeService.createEnum<mediaTypeEnum>(mediaTypeEnum, 'mediaTypeEnum');
