import { BaseActions } from '../../Global/actions/baseActions';

export class InstImageActions extends BaseActions {
    /**
     * Получаем id хэштега по хэштегу
     */
    static getHashtagId = (hashtag: string | null) => {
        let formData = new FormData();
        formData.append('q', `${hashtag}`);

        return InstImageActions.apiResource(
            'ig_hashtag_search',
            formData,
        );
    }

    /**
     * Получаем фотки по хэштегу
     */
    static getImg = (hashtagId: string | null) => {
        let formData = new FormData();
        formData.append('fields', 'media_url, media_type');

        return InstImageActions.apiResource(
            `${hashtagId}/recent_media`,
            formData,
        );
    }
}
