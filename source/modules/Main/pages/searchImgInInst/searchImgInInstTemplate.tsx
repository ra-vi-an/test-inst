/* eslint-disable no-undef */
import { FlexBox } from '@thewhite/react-flex-layout';
import * as React from 'react';
import Toast from '../../../Global/components/toast/toast';
import { SearchImgInInstParams } from './searchImgInInstComponent';

const searchImgInInstTemplate = (context: SearchImgInInstParams): JSX.Element => {
    return (
        <FlexBox
            column={'ctr'}
            className={'search-img-in-inst'}
        >
            <h2
                className={'search-img-in-inst__title'}
            >
                Поиск фотографий Инстаграма по хэштегам
            </h2>
            <FlexBox
                row={'start'}
                className={'search-img-in-inst__body'}
            >
                <FlexBox
                    column={'start'}
                    className={'search-img-in-inst__column'}
                >
                    <FlexBox
                        className={'search-img-in-inst__column-top'}
                        column={'start'}
                    >
                        <h3
                            className={'search-img-in-inst__column-top-title'}
                        >
                            Первый хэштег
                        </h3>
                        <FlexBox
                            row={'sb start'}
                            className={'search-img-in-inst__column-top-search'}
                        >
                            <input
                                value={context.state.firstHashtag ?? ''}
                                className={'search-img-in-inst__input'}
                                onChange={e => context.updateState(e?.target?.value ?? null, 'firstHashtag')}
                            />
                            <button
                                onClick={() => context.startingSearch(context.state.firstHashtag, 'firstHashtag')}
                                className={'search-img-in-inst__button'}
                            >
                                <h3>
                                    Найти
                                </h3>
                            </button>
                        </FlexBox>
                    </FlexBox>
                    <div
                        className={'search-img-in-inst__column-img'}
                    >
                        {
                            context.state.firstHashtagImages.map((item: any, index: number) =>
                                <img
                                    key={index}
                                    src={item.media_url}
                                    className={'search-img-in-inst__img'}
                                />,
                            )
                        }
                    </div>
                </FlexBox>
                <FlexBox
                    column={'start'}
                    className={'search-img-in-inst__column'}
                >
                    <FlexBox
                        column={'start'}
                        className={'search-img-in-inst__column-top'}
                    >
                        <h3
                            className={'search-img-in-inst__column-top-title'}
                        >
                            Второй хэштег
                        </h3>
                        <FlexBox
                            row={'sb start'}
                            className={'search-img-in-inst__column-top-search'}
                        >
                            <input
                                value={context.state.secondHashtag ?? ''}
                                className={'search-img-in-inst__input'}
                                onChange={e => context.updateState(e?.target?.value ?? null, 'secondHashtag')}
                            />
                            <button
                                onClick={() => context.startingSearch(context.state.secondHashtag, 'secondHashtag')}
                                className={'search-img-in-inst__button'}
                            >
                                <h3>
                                    Найти
                                </h3>
                            </button>
                        </FlexBox>
                    </FlexBox>
                    <div
                        className={'search-img-in-inst__column-img'}
                    >
                        {
                            context.state.secondHashtagImages.map((item: any, index: number) =>
                                <img
                                    key={index}
                                    src={item.media_url}
                                    className={'search-img-in-inst__img'}
                                />,
                            )
                        }
                    </div>
                </FlexBox>
                <FlexBox
                    column={'start'}
                    className={'search-img-in-inst__column'}
                >
                    <div
                        className={'search-img-in-inst__column-top'}
                    >
                        <h3
                            className={'search-img-in-inst__column-top-title'}
                        >
                            Совпадающие хэштеги
                        </h3>
                        {
                            !context.state.sortedImages.length &&
                            <h4
                                className={'search-img-in-inst__column-img-result'}
                            >
                                Совпадений не найдено
                            </h4>
                        }
                    </div>
                    {
                        !!context.state.sortedImages.length &&
                            <div
                                className={'search-img-in-inst__column-img'}
                            >
                                {
                                    context.state.sortedImages.map((item: string, index: number) =>
                                        <img
                                            key={index}
                                            src={item}
                                            className={'search-img-in-inst__img'}
                                        />,
                                    )
                                }
                            </div>
                    }
                </FlexBox>
            </FlexBox>
            {
                context.state.loading &&
                <FlexBox
                    row={'ctr'}
                    className={'search-img-in-inst__loading'}
                >
                    <img
                        src={'../assets/images/loading.gif'}
                    />
                </FlexBox>
            }
            <Toast
                status={context.state.statusToast}
                showToast={context.state.showToast}
                massage={context.state.toastMessage}
            />
        </FlexBox>
    );
};

export default searchImgInInstTemplate;
