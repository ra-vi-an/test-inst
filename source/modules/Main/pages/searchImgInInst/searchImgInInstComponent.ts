/* eslint-disable no-undef */
import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { inject, observer } from 'mobx-react';
import { GlobalStore } from '../../../../store/globalStore';
import STATUS from '../../../Global/enums/status';
import TOAST from '../../../Global/enums/toast';
import { InstImageActions } from '../../actions/instImageActions';
import MEDIA_TYPE from '../../enums/mediaType';
import searchImgInInstTemplate from './searchImgInInstTemplate';

export interface SearchImgInInstProps {
    globalStore?: GlobalStore;
}

export interface SearchImgInInstState {
    [value: string]: any;

    loading: boolean;
    showToast: boolean;
    sortedImages: string[];
    firstHashtagImages: any[];
    secondHashtagImages: any[];
    statusToast: string | null;
    firstHashtag: string | null;
    toastMessage: string | null;
    secondHashtag: string | null;
}

export interface SearchImgInInstParams extends WsReactBaseComponentInterface {
    props: SearchImgInInstProps;
    state: SearchImgInInstState;
    timeInterval: number;
    showToastTime: number;
    intervalRenewal: number;

    interval(): void;
    startingSearch(hashtag: string | null, fieldInState?: string): void;
}

@inject('globalStore')
@observer
class SearchImgInInst extends WsReactBaseComponent<SearchImgInInstProps, SearchImgInInstState> implements SearchImgInInstParams {
    intervalRenewal: number = 0;
    timeInterval: number = 10000;
    showToastTime: number = 3000;
    state: SearchImgInInstState = {
        loading: false,
        showToast: false,
        sortedImages: [],
        statusToast: null,
        toastMessage: null,
        firstHashtag: 'cat',
        secondHashtag: 'dog',
        firstHashtagImages: [],
        secondHashtagImages: [],
    }

    /**
     * Запуск поиска фотографий по хэштегу
     */
    startingSearch = (hashtag: string, fieldInState: string) => {
        this.spinner(true);
        this.getIdByHashtag(hashtag, fieldInState);
        if (this.state.firstHashtag && this.state.secondHashtag) this.interval();
    };

    /**
     * Получаем id хэштега по хэштегу
     */
    getIdByHashtag = (hashtag: string | null, fieldInState?: string) => {
        if (!fieldInState || !this.checkAvailabilityHashtag(fieldInState)) return;

        InstImageActions.getHashtagId(hashtag)
            .then((res: any) => {
                if (this.checkingTheRequestForErrors(res)) return;
                this.getImagesById(res, fieldInState);
            });
    }

    /**
     * Проверка на наличие хэштега в поле input'а
     */
    checkAvailabilityHashtag = (fieldInState: string): boolean => {
        const fieldHashtagInState: string = this.state[fieldInState];
        const fieldArrImagesInState: string = `${fieldInState}Images`;
        if (fieldHashtagInState) return true;

        this.setState({ [fieldArrImagesInState]: [] });
        this.spinner(false);
        clearInterval(this.intervalRenewal);
        return false;
    };

    /**
     * Получаем фотографии по id хэштега
     */
    getImagesById = (dataImages: any, fieldInState?: string): any => {
        if (!dataImages) return;
        const sortedImages: any[] = [];

        InstImageActions.getImg(dataImages.data[0].id ?? '')
            .then((response) => {
                if (this.checkingTheRequestForErrors(response)) return;
                response.data.forEach((item: any) => {
                    if (item.media_type === MEDIA_TYPE.IMAGE) {
                        sortedImages.push(item);
                    }
                });
                this.addImgInState(sortedImages, fieldInState);
            });
    }

    /**
     * Складываем фотографии в нужое поле
     */
    addImgInState = (sortedImages: any[], fieldInState?: string) => {
        if (!sortedImages.length) return;
        const fieldArrImagesInState: string = `${fieldInState}Images`;

        this.setState({ [fieldArrImagesInState]: sortedImages });
        this.spinner(false);
        this.filterHashtag();
    }

    /**
     * Фильтруем массивы фотографий по id
     */
    filterHashtag = () => {
        if (!this.checkAvailabilityImages()) return;
        let sortedImages: string[] = this.state.sortedImages;

        this.state.firstHashtagImages.forEach((itemFirst) => {
            this.state.secondHashtagImages.forEach((itemSecond) => {
                if (itemFirst.id === itemSecond.id) {
                    sortedImages.unshift(itemFirst.media_url);
                }
            });
        });
        this.setState({ sortedImages: [...new Set(sortedImages)] });
    };

    /**
     * Интервал запроса фотографий
     */
    interval = () => {
        if (!this.checkAvailabilityImages()) return;

        this.intervalRenewal = setInterval(
            () => {
                this.getIdByHashtag(this.state.firstHashtag, 'firstHashtag');
                this.getIdByHashtag(this.state.secondHashtag, 'secondHashtag');
            },
            this.timeInterval,
        );
    }

    /**
     * Тост при ошибке
     */
    toast = (toastMessage: string) => {
        this.spinner(false);
        clearInterval(this.intervalRenewal);
        this.setState({
            toastMessage,
            showToast: true,
            statusToast: TOAST.ERROR.value,
        });
        setTimeout(
            () => this.setState({ showToast: false }),
            this.showToastTime,
        );
    };

    /**
     * Проверка запроса на наличие ошибок
     */
    checkingTheRequestForErrors = (data: any): boolean => {
        if ((Object.keys(data))[0] !== STATUS.ERROR) return false;
        this.toast(data.error.message ?? '');
        return true;
    }

    /**
     * Проверка на наличие фотографий в state
     */
    checkAvailabilityImages = (): boolean => {
        return !!this.state.firstHashtagImages.length || !!this.state.secondHashtagImages.length;
    }

    /**
     * Спиннер загрузки
     */
    spinner = (loading: boolean) => {
        this.setState({ loading });
    }

    render(): false | JSX.Element {
        return searchImgInInstTemplate(this);
    }
}

export { SearchImgInInst };
