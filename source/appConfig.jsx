import DevTools from 'mobx-react-devtools';
import React from 'react';
import { ToastContainer } from 'react-toastify';
import MainRouter from './routers/mainRouter.tsx';

export default function AppConfig() {
    return (
        <>
            {process.env.NODE_ENV === 'development' && <DevTools />}
            <MainRouter />
            <ToastContainer
                toastClassName="global-toast"
                // position="BOTTOM_RIGHT"
            />
        </>
    );
}
