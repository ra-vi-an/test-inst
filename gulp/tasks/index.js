import gulp from 'gulp';
import config from '../config';

function indexHtml() {
    return gulp.src(config.indexHtmlPath)
        .pipe(gulp.dest(config.DIST_PATH))
}

export { indexHtml };
